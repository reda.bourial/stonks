package main

import (
	_ "gitlab.com/reda.bourial/stonks/brokers/xtb"
	"gitlab.com/reda.bourial/stonks/core"
)

func main() {
	core.Start()
}
