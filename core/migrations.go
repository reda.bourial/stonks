package core

import (
	"database/sql"
	"fmt"

	"gitlab.com/reda.bourial/log4g"
)

type Migrations []Migration

func RegisterMigrations(appName string, m Migrations) {
	migrations[appName] = m
}

func CheckIfTableExists(db *sql.DB, tableName string) (exists bool, err error) {
	results, err := db.Query(`
		SELECT 
		COUNT(1)
		FROM 
		   information_schema.TABLES 
		WHERE 
			TABLE_TYPE LIKE 'BASE TABLE' AND
			TABLE_NAME = ?;
	`, tableName)
	defer results.Close()
	if err != nil {
		return false, err
	}
	results.Next()
	if err != nil {
		return false, err
	}
	var res int
	err = results.Scan(&res)
	if err != nil {
		return false, err
	}
	return res != 0, nil
}

func IsMigrationApplied(idx int, plugin string, db *sql.DB) (isApplied bool) {
	tableExists, err := CheckIfTableExists(db, "core__migrations")
	PanicIfError(err)
	if !tableExists {
		return false
	}
	results, err := db.Query(`
	SELECT 
	COUNT(1)
	FROM core__migrations
	WHERE 
		idx = ?
	AND 
	    plugin = ? ;
	`, idx, plugin)
	PanicIfError(err)
	defer results.Close()

	results.Next()
	PanicIfError(err)
	var res int
	err = results.Scan(&res)
	PanicIfError(err)

	return res != 0
}

func ApplyMigration(logger log4g.Logger, migrationIdx int, plugin string, m Migration, db *sql.DB) {
	err := m.Apply(logger, db)
	PanicIfError(err)
	_, err = db.Exec(`
	INSERT INTO stonks.core__migrations 
	(plugin, idx) VALUES 
	(?, ?);
	`, plugin, migrationIdx)
	PanicIfError(err)
}

func RunPluginMigrations(logger log4g.Logger, plugin string, migrationList []Migration, db *sql.DB) {
	for idx, m := range migrationList {
		migrationName := fmt.Sprintf("%s_%d", plugin, idx)
		if !IsMigrationApplied(idx, plugin, db) {
			ApplyMigration(logger, idx, plugin, m, db)
			logger.Info("applied", migrationName)
		} else {
			logger.Trace("skipped", migrationName)
		}
	}
}

func RunMigrations(logger log4g.Logger) {
	db, err := GetDatabase(nil)
	PanicIfError(err)
	defer func() {
		if r := recover(); r != nil {
			logger.Error("An error occurred", r)
		}
	}()
	for plugin, migrationList := range migrations {
		RunPluginMigrations(logger, plugin, migrationList, db)
	}
	logger.Info("All available migrations applied")

}
