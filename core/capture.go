package core

import (
	"context"
	"os"
	"sync"
)

type OpenFile struct {
	*os.File
	sync.Mutex
}

type DiskCaptureContext struct {
	Context    context.Context `yagclif:"omit"`
	FolderPath string
	openFiles  map[string]*OpenFile `yagclif:"omit"`
}

func (ctx DiskCaptureContext) Init() error {
	//f, err := os.OpenFile("text.log",
	//	os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	return nil
}
