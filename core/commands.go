package core

import (
	"fmt"
	"time"

	"gitlab.com/reda.bourial/log4g"
	"gitlab.com/reda.bourial/yagclif/v2"
)

var routes = yagclif.Routes{
	{
		Name:        "stall",
		Description: "stalls the app (used for dev)",
		Callback: func(remainingArgs []string) {
			for true {
				time.Sleep(5000 * time.Second)
			}
		},
	},
	{
		Name:        "migrate",
		Description: "apply migrations",
		Callback: func(remainingArgs []string) {
			if len(remainingArgs) > 0 {
				panic(fmt.Sprint("unknown arguments ", remainingArgs))
			}
			logger := log4g.NewConsoleLogger().PrependTime()
			logger.Info("Connecting to database")
			_, err := GetDatabase(nil)
			if err != nil {
				panic(err)
			}
			logger.Info("Connected to database")
			RunMigrations(logger)
		},
	},
	{
		Name:        "version",
		Description: "print out version",
		Callback: func(remainingArgs []string) {
			if len(remainingArgs) > 0 {
				panic(fmt.Sprint("unknown arguments", remainingArgs))
			}
			fmt.Println("Stonks version", Version)
		},
	},
}

func PanicIfError(err error) {
	if err != nil {
		fmt.Println(err)
		panic(err)
	}
}

func RegisterRoutes(appName string, appDescription string, routes yagclif.Routes) {
	newApp := yagclif.NewCliApp(appName, appDescription, true)
	_, err := newApp.AddRoutes(routes)
	PanicIfError(err)
	err = app.AddApp(newApp)
	PanicIfError(err)
}

func Start() {
	coreApp := yagclif.NewCliApp("core", "core commands", true)
	_, err := coreApp.AddRoutes(routes)
	PanicIfError(err)
	err = app.AddApp(coreApp)
	PanicIfError(err)
	err = app.Run()
	if err != nil {
		fmt.Println(err)
	}
}
