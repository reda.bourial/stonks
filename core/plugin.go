package core

import (
	"gitlab.com/reda.bourial/yagclif/v2"
)

type Plugin struct {
	Name          string
	Description   string
	Version       string
	Commands      yagclif.Routes
	BrokerFactory BrokerFactory
	Migrations    Migrations
}

var plugins = []Plugin{}

func RegisterPlugin(p Plugin) {
	if p.Commands != nil {
		RegisterRoutes(p.Name, p.Description, p.Commands)
	}
	if p.BrokerFactory != nil {
		RegisterBroker(p.Name, p.BrokerFactory)
	}
	if p.Migrations != nil {
		RegisterMigrations(p.Name, p.Migrations)
	}
	plugins = append(plugins, p)
}
