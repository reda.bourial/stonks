module gitlab.com/reda.bourial/stonks

go 1.15

require (
	github.com/go-sql-driver/mysql v1.6.0
	github.com/google/uuid v1.3.0 // indirect
	github.com/gorilla/websocket v1.5.0
	github.com/tidwall/gjson v1.14.0
	gitlab.com/reda.bourial/catch v1.0.4
	gitlab.com/reda.bourial/log4g v1.0.3
	gitlab.com/reda.bourial/yagclif/v2 v2.1.6
)
