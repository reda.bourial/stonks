package xtb

import (
	"fmt"
	"sync"
	"time"

	"github.com/tidwall/gjson"
	"gitlab.com/reda.bourial/stonks/core"
)

type XTBCmdConnection *core.BufferedConnection

type Account struct {
	broker           *Broker
	Login            string
	Password         string
	hash             uint64
	isDemo           bool
	CmdConnection    *core.BufferedConnection
	StreamConnection *core.BufferedConnection
	ssid             string
	cmdMutex         *sync.Mutex
	streamMutex      *sync.Mutex
}

func (a *Account) Broker() core.Broker {
	return a.broker
}

func (a *Account) cmdLogin() (gjson.Result, error) {
	r, err := a.unsafeWriteToCmdCon(Request{
		Command: "login",
		Arguments: LoginArgs{
			UserId:   a.Login,
			Password: a.Password,
			AppId:    "42",
			AppName:  "stonks",
		},
	})
	if err != nil {
		return gjson.Result{}, err
	} else if !r.Get("status").Bool() {
		return gjson.Result{}, fmt.Errorf("cmd error %s", r)
	}
	a.ssid = r.Get("streamSessionId").String()
	return r, err
}

func (a *Account) IsAlive() bool {
	return a.Error() == nil
}

func (a *Account) Error() error {
	if err := a.CmdConnection.Error(); err != nil {
		return a.CmdConnection.Error()
	}
	return a.StreamConnection.Error()
}

func (a *Account) Init() error {
	var mainHost string
	if a.isDemo {
		mainHost = "demo"
	} else {
		mainHost = "real"
	}
	url := GetHosts()[mainHost]
	CmdConnection, err := core.GetBufferedConnection(url)
	if err != nil {
		return err
	}
	a.CmdConnection = CmdConnection
	_, err = a.cmdLogin()
	if err != nil {
		return err
	}
	// Stream socket
	streamUrl := GetHosts()[fmt.Sprintf("%sStream", mainHost)]
	a.StreamConnection, err = core.GetBufferedConnection(streamUrl)
	if err != nil {
		return err
	}
	cmdTicker := time.NewTicker(5 * time.Second)
	streamTicker := time.NewTicker(5 * time.Second)
	go func() {
		for _ = range cmdTicker.C {
			a.writeToCmdCon(map[string]string{
				"command": "ping",
			})
		}
	}()
	go func() {
		for _ = range streamTicker.C {
			a.writeToStreamCon(map[string]string{
				"command":         "ping",
				"streamSessionId": a.ssid,
			})
		}
	}()
	return nil
}

func (a *Account) Reconnect() error {
	time.Sleep(1 * time.Second)
	a.cmdMutex.Lock()
	defer a.cmdMutex.Unlock()
	a.streamMutex.Lock()
	defer a.streamMutex.Unlock()
	err := a.CmdConnection.Reconnect()
	if err != nil {
		return err
	}
	_, err = a.cmdLogin()
	if err != nil {
		return err
	}
	err = a.StreamConnection.Reconnect()
	return err
}

func (a *Account) unsafeWriteToStreamCon(obj interface{}) error {
	if err := a.StreamConnection.Error(); err != nil {
		return err
	}
	return a.StreamConnection.Write(obj)
}

func (a *Account) unsafeWriteToCmdCon(obj interface{}) (gjson.Result, error) {
	timeout := 2 * time.Second
	if err := a.CmdConnection.Error(); err != nil {
		return gjson.Result{}, err
	}
	err := a.CmdConnection.Write(obj)
	if err != nil {
		return gjson.Result{}, err
	}
	startTime := time.Now()
	messages, err := a.CmdConnection.GetJsonMessages()
	for len(messages) == 0 && err == nil && time.Now().Sub(startTime) < timeout {
		time.Sleep(50 * time.Microsecond)
		messages, err = a.CmdConnection.GetJsonMessages()
	}

	if err != nil {
		return gjson.Result{}, err
	} else if time.Now().Sub(startTime) >= timeout {
		return gjson.Result{}, fmt.Errorf("command timeout")
	}
	return messages[0], nil
}

func (a *Account) writeToStreamCon(obj interface{}) error {
	a.streamMutex.Lock()
	defer a.streamMutex.Unlock()
	return a.unsafeWriteToStreamCon(obj)
}

func (a *Account) writeToCmdCon(obj interface{}) (gjson.Result, error) {
	a.cmdMutex.Lock()
	defer a.cmdMutex.Unlock()
	return a.unsafeWriteToCmdCon(obj)
}

func (a *Account) subscribeToTicker(ticker string) error {
	return a.writeToStreamCon(map[string]interface{}{
		"command":         "getTickPrices",
		"streamSessionId": a.ssid,
		"symbol":          ticker,
		"minArrivalTime":  1,
		"maxLevel":        5,
	})
}
func (a *Account) getAllSymbols() gjson.Result {
	message, err := a.writeToCmdCon(Request{
		Command:   "getAllSymbols",
		Arguments: map[string]string{},
	})
	core.PanicIfError(err)
	return message
}
func (a *Account) GetInstruments() []core.Instrument {
	// TODO READ from database
	return nil
}
func (a *Account) Name() string {
	return a.Login
}

func (a *Account) Hash() uint64 {
	return a.hash
}

func (a *Account) Currency() string {
	//TODO
	return "USD"
}

func (a *Account) IsDemo() bool {
	return a.isDemo
}
func (a *Account) Order(ticker string, price float64, size core.Number) error {
	//TODO
	return nil
}
func (a *Account) GetPendingOrders() []core.Order {
	//TODO
	return nil
}
func (a *Account) GetAllPositions(from time.Time, to time.Time) chan core.Position {
	//TODO
	return nil
}
func (a *Account) GetOpenPositions() chan core.Position {
	//TODO
	return nil
}
func (a *Account) Value() float64 {
	//TODO
	return 0
}

func AccountFactory(json gjson.Result, isDemo bool) (a core.Account, isDefault bool, err error) {
	login, password := json.Get("login").String(), json.Get("password").String()
	hash := core.Hash(login)
	account := Account{
		Login:       login,
		Password:    password,
		isDemo:      isDemo,
		cmdMutex:    &sync.Mutex{},
		streamMutex: &sync.Mutex{},
		hash:        hash,
	}
	err = account.Init()
	return &account, json.Get("default").Bool(), err
}
