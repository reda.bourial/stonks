build:
	go build .

test:
	go test -v -coverprofile=coverage.out  -timeout 30s -run .

coverage:
	go tool cover -html=coverage.out

ci_coverage:
	go tool cover -func=coverage.out
	
ci_coverage_check:
	make ci_coverage | grep "total:" |grep "100.0%"

pre_review: start test coverage ci_coverage_check


pre_review: start test lint ci_coverage_check

docker_cleanup:
	docker rm -f $$(docker ps  | grep stonks | rev | cut -d" " -f1 |rev)

docker_up:
	SSH_PRIVATE_KEY="$$(cat ~/.ssh/id_rsa)" docker-compose build
	docker-compose up -d

docker_rebuild: docker_up

docker_console:
	docker exec -it stonks_web_1  /bin/bash
