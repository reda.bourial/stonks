package core

import (
	"time"

	"github.com/tidwall/gjson"
)

type Account interface {
	Broker() Broker
	IsAlive() bool
	Error() error
	Reconnect() error
	Name() string
	Hash() uint64
	Currency() string
	IsDemo() bool
	Order(ticker string, price float64, size Number) error
	GetPendingOrders() []Order
	GetAllPositions(from time.Time, to time.Time) chan Position
	GetOpenPositions() chan Position
	Value() float64
	GetInstruments() []Instrument
}

type Broker interface {
	GetAccounts() map[uint64]Account
	GetDefaultAccount() Account
	GetDefaultDemo() Account
}

type BrokerFactory func(broker gjson.Result) (Broker, error)

func RegisterBroker(name string, bf BrokerFactory) {
	app.Brokers[name] = bf
}

func GetBroker(name string) (Broker, error) {
	config := GetConfig(name)
	return app.Brokers[name](config)
}
