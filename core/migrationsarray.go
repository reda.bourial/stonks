package core

var migrations = map[string]Migrations{
	"stonks": {
		MakeMigration(
			[]string{`
			create table core__migrations
			(
				plugin           VARCHAR(255),
				idx              int,
				applied_on       TIMESTAMP not null DEFAULT CURRENT_TIMESTAMP,
				  PRIMARY KEY (idx, plugin)
			);
		   `},
		),
		MakeMigration(
			[]string{
				`create table core__logs
			(
				id int auto_increment,
				create_at timestamp default CURRENT_TIMESTAMP not null,
				run_id UUID not null,
				plugin VARCHAR(16) not null,
				data JSON not null,
				constraint core__logs_pk
					primary key (id)
			);`,
				`
			create index core__logs_run_id_plugin_index
				on core__logs (run_id, plugin);
			`,
			},
		),
		MakeMigration(
			[]string{`
			alter table core__migrations modify plugin varchar(16) not null;
		   `},
		),
		MakeMigration(
			[]string{
				`
			alter table core__logs engine=MYISAM;
		   `,
				`
			alter table core__logs
				add code VARCHAR(32) not null after plugin;
			`},
		),
		MakeMigration(
			[]string{
				`
				drop index core__logs_run_id_plugin_index on core__logs;
				`,
				`
				create index core__logs_run_id_plugin_code_index
					on core__logs (run_id, plugin, code);
				`,
			},
		),
	},
}
