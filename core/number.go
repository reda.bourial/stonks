package core

const factor = 1000000

type Number int64

func ToFloat(m Number) float64 {
	return (float64(m) / float64(factor))
}

func fromFloat(n float64) Number {
	return Number(n * factor)
}
