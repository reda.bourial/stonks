package core

import (
	"io/ioutil"
	"os"
	"sync"

	"github.com/tidwall/gjson"
)

var configPath = "stonks.json"
var config *gjson.Result
var configMutex sync.Mutex

func GetConfig(NameSpace string) gjson.Result {
	if config == nil {
		configMutex.Lock()
		defer configMutex.Unlock()
		f, err := os.Open(configPath)
		if err != nil {
			panic(err)
		}
		data, err := ioutil.ReadAll(f)
		if err != nil {
			panic(err)
		}
		f.Close()
		res := gjson.Parse(string(data))
		config = &res
	}
	return config.Get(NameSpace)
}
