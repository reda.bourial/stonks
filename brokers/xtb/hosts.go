package xtb

func GetHosts() map[string]string {
	return map[string]string{
		"real":       "wss://ws.xtb.com/real",
		"realStream": "wss://ws.xtb.com/realStream",
		"demo":       "wss://ws.xtb.com/demo",
		"demoStream": "wss://ws.xtb.com/demoStream",
	}
}
