package core

import "github.com/tidwall/gjson"

type TickPackage struct {
	Tick
	Error error
}

type TickStream chan TickPackage

type Instrument interface {
	GetTicker() string
	GetPipValue() float32
	GetCurrency() string
	GetLeverage() float32
	MinPosition() (volume float64, money float64)
	CanShort() bool
	TickValue() float32
	TickSize() float32
	Raw() gjson.Result
}
