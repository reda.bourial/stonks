package xtb

import (
	"database/sql"

	"github.com/tidwall/gjson"
)

type Instrument struct {
	Id             int64
	Symbol         string
	groupName      string
	description    string
	currency       string
	currencyProfit string
	categoryName   string
	marginMode     int64
	leverage       float64
	lotMin         float64
	tickSize       float64
	tickValue      float64
	swapLong       float64
	swapShort      float64
	longOnly       bool
	rawJson        gjson.Result
	marketHours    gjson.Result
}

func getInstrumentFromRow(r *sql.Rows) (*Instrument, error) {
	i := Instrument{}
	rawJson, marketHours := "", ""
	err := r.Scan(
		&i.Id,
		&i.Symbol,
		&i.groupName,
		&i.description,
		&i.currency,
		&i.currencyProfit,
		&i.categoryName,
		&i.marginMode,
		&i.leverage,
		&i.lotMin,
		&i.tickSize,
		&i.tickValue,
		&i.swapLong,
		&i.swapShort,
		&i.longOnly,
		&rawJson,
		&marketHours,
	)
	if err != nil {
		return nil, err
	}
	i.rawJson = gjson.Parse(rawJson)
	i.marketHours = gjson.Parse(marketHours)
	return &i, err
}

func getInstrument(symbol string, db *sql.DB) (*Instrument, error) {
	rows, err := db.Query(`
	SELECT
    	id,
    	symbol,
    	group_name,
    	description,
    	currency,
    	currency_profit,
    	category_name,
    	margin_mode,
    	leverage,
    	lot_min,
    	tick_size,
    	tick_value,
    	swap_long,
    	swap_short,
    	long_only,
    	rawJson,
    	marketHours
	FROM 
		xtb__tickers__data
	WHERE symbol=?;
	`, symbol)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	if rows.Next() {
		return getInstrumentFromRow(rows)
	}
	return nil, nil
}

func getInstruments(db *sql.DB) ([]Instrument, error) {
	rows, err := db.Query(`
	SELECT
    	id,
    	symbol,
    	group_name,
    	description,
    	currency,
    	currency_profit,
    	category_name,
    	margin_mode,
    	leverage,
    	lot_min,
    	tick_size,
    	tick_value,
    	swap_long,
    	swap_short,
    	long_only,
    	rawJson,
    	marketHours
	FROM 
		xtb__tickers__data;
	`)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	instruments := []Instrument{}
	for rows.Next() {
		i, err := getInstrumentFromRow(rows)
		if err != nil {
			return nil, err
		}
		instruments = append(instruments, *i)
	}
	return instruments, nil
}

func getInstrumentsAsJson() ([]gjson.Result, error) {
	return nil, nil
}

func updateOrCreateInstrument(i *Instrument, db *sql.DB) (created bool, err error) {
	var query string
	if i.Id == 0 {
		query = `
				INSERT INTO
				stonks.xtb__tickers__data
				    (
				     group_name,
				     description,
				     currency,
				     currency_profit,
				     category_name,
				     margin_mode,
				     leverage,
				     lot_min,
				     tick_size,
				     tick_value,
				     swap_long,
				     swap_short,
				     long_only,
				     rawJson,
				     marketHours,
				     symbol
				     ) VALUES (
				     ?,
				     ?,
				     ?,
				     ?,
				     ?,
				     ?,
				     ?,
				     ?,
				     ?,
				     ?,
				     ?,
				     ?,
				     ?,
				     ?,
				     ?,
					 ?
				);
			`
	} else {
		query = `
				UPDATE stonks.xtb__tickers__data t 
				SET 
				t.group_name = ?,
				t.description = ?,
				t.currency = ?,
				t.currency_profit = ?,
				t.category_name = ?,
				t.margin_mode = ?,
				t.leverage = ?,
				t.lot_min = ?,
				t.tick_size = ?,
				t.tick_value = ?,
				t.swap_long = ?,
				t.swap_short = ?,
				t.long_only = ?,
				t.rawJson = ?,
				t.marketHours = ?
				WHERE t.symbol = ?;
		`
	}
	result, err := db.Exec(
		query,
		i.groupName,
		i.description,
		i.currency,
		i.currencyProfit,
		i.categoryName,
		i.marginMode,
		i.leverage,
		i.lotMin,
		i.tickSize,
		i.tickValue,
		i.swapLong,
		i.swapShort,
		i.longOnly,
		i.rawJson.String(),
		i.marketHours.String(),
		i.Symbol,
	)
	if err == nil && i.Id == 0 {
		i.Id, err = result.LastInsertId()
		return true, err
	}
	return false, err
}

func syncInstrument(value gjson.Result, db *sql.DB) (oldValue *Instrument, err error) {
	i := &Instrument{
		Symbol:         value.Get("symbol").String(),
		groupName:      value.Get("groupName").String(),
		description:    value.Get("description").String(),
		currency:       value.Get("currency").String(),
		currencyProfit: value.Get("currencyProfit").String(),
		categoryName:   value.Get("categoryName").String(),
		marginMode:     value.Get("marginMode").Int(),
		leverage:       value.Get("leverage").Float(),
		lotMin:         value.Get("lotMin").Float(),
		tickSize:       value.Get("tickSize").Float(),
		tickValue:      value.Get("tickValue").Float(),
		swapLong:       value.Get("swapLong").Float(),
		swapShort:      value.Get("swapShort").Float(),
		longOnly:       value.Get("longOnly").Bool(),
		rawJson:        value,
		marketHours:    gjson.Parse("{}"),
	}
	oldValue, err = getInstrument(i.Symbol, db)
	if err != nil {
		return
	} else if oldValue != nil {
		i.Id = oldValue.Id
	}
	_, err = updateOrCreateInstrument(i, db)
	return oldValue, err
}

func (i *Instrument) GetTicker() string {
	return i.Symbol
}
func (i *Instrument) GetPipValue() float64 {
	return i.GetPipValue()
}
func (i *Instrument) GetCurrency() string {
	return i.currency
}
func (i *Instrument) GetLeverage() float64 {
	return i.leverage
}
func (i *Instrument) CanShort() bool {
	return !i.longOnly
}

func (i *Instrument) String() string {
	return i.GetTicker()
}
