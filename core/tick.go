package core

import (
	"time"
)

type Tick interface {
	Timestamp() time.Time
	Ask() int64
	Bid() int64
	AskF() float64
	BidF() float64
	GetPipValue() float64
	GetInverse() float64
	AskVolume() int64
	BidVolume() int64
	Instrument() Instrument
}
