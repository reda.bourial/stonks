package xtb

import (
	"gitlab.com/reda.bourial/stonks/core"
)

func init() {
	core.RegisterPlugin(core.Plugin{
		Name:          "xtb",
		Description:   "plugin for https://xtb.com/",
		Version:       "2.5.0",
		Commands:      commands,
		BrokerFactory: BrokerFactory,
		Migrations:    migrations,
	})
}
