package core

import (
	"gitlab.com/reda.bourial/yagclif/v2"
)

const Version = "0.0.1"

type StonksApp struct {
	*yagclif.App
	Brokers map[string]BrokerFactory
}

var app = StonksApp{
	yagclif.NewCliApp("Go Stonks", "The more smarter way to loose money", false),
	map[string]BrokerFactory{},
}
