package core

import "time"

type Position interface {
	// Id of the position at the broker
	Id() string
	// Faster implementation of id
	IdHash() int
	// Open price
	Open() float64
	// close price
	Closed() float64
	OpenedAt() time.Time
	ClosedAt() (timestamp time.Time, closed bool)
	Size() int
	GrossPnL() float64
	NetPnL() float64
	PnlDiff() float64
	Instrument() Instrument
}

type Order interface {
	// Order Id at the broker
	Id() string
	// Faster implementation of id
	IdHash() int
	Size() int
	// When it opened
	OpenAt() time.Time
	RequestedPrice() float64
	// Cancels the order
	Cancel() error
	// Instrument
	Instrument() Instrument
}
