package core

import (
	"database/sql"

	"gitlab.com/reda.bourial/log4g"
)

type VanillaMigration []string

func (migration VanillaMigration) Apply(logger log4g.Logger, db *sql.DB) error {
	var err error
	for _, q := range migration {
		_, err = db.Exec(q)
		if err != nil {
			return err
		}
	}
	return nil
}

func MakeMigration(queries []string) Migration {
	return VanillaMigration(queries)
}

type Migration interface {
	Apply(logger log4g.Logger, db *sql.DB) error
}
