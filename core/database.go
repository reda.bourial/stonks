package core

import (
	"database/sql"
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type Packet struct {
	time time.Time
	data []interface{}
}

type StreamPacket struct {
	Packets []Packet
	err     error
}

type DatabaseOptions struct {
	Multistatement bool
}

type Timeserie interface {
	Insert(time time.Time, data []interface{})
	getFrom(time time.Time) StreamPacket
	streamFrom(time time.Time) chan StreamPacket
}

func getDataSourceName(opts *DatabaseOptions) string {
	core := GetConfig("core").Get("database")
	uri := fmt.Sprintf(
		"%s:%s@(%s:%s)/%s",
		core.Get("username").Str,
		core.Get("password").Str,
		core.Get("address").Str,
		core.Get("port_num").Str,
		core.Get("database").Str,
	)
	if !opts.Multistatement {
		return uri
	}
	return fmt.Sprintf("%s?multiStatements=true", uri)
}

func GetDatabase(opts *DatabaseOptions) (*sql.DB, error) {
	if opts == nil {
		opts = &DatabaseOptions{}
	}
	dataSourceName := getDataSourceName(opts)
	return sql.Open("mysql", dataSourceName)
}
