package xtb

import (
	"bytes"
	"container/list"
	"fmt"

	"github.com/tidwall/gjson"
	"gitlab.com/reda.bourial/stonks/core"
)

type Tick struct {
	id        int64
	tickerId  int64
	timestamp int64
	ask       float64
	askVol    int64
	bid       float64
	bidVol    int64
	level     int64
	quoteId   int64
}

func (tick Tick) toSqlValues() string {
	return fmt.Sprintf(`(
		%d ,
		%d,
		%f ,
		%d ,
		%f ,
		%d ,
		%d ,
		%d
	)`,
		tick.timestamp,
		tick.tickerId,
		tick.ask,
		tick.askVol,
		tick.bid,
		tick.bidVol,
		tick.level,
		tick.quoteId,
	)
}
func getTickFromMessage(symbolToIdMap map[string]int64, message gjson.Result) (tick Tick, exists bool) {
	if message.Get("command").String() == "tickPrices" {
		data := message.Get("data")
		symbol := data.Get("symbol").String()
		id := symbolToIdMap[symbol]
		return Tick{
			timestamp: data.Get("timestamp").Int(),
			tickerId:  id,
			ask:       data.Get("ask").Float(),
			askVol:    data.Get("askVolume").Int(),
			bid:       data.Get("bid").Float(),
			bidVol:    data.Get("bidVolume").Int(),
			level:     data.Get("level").Int(),
			quoteId:   data.Get("quoteId").Int(),
		}, true
	}
	return Tick{}, false
}

func getValuesFromMessages(symbolToIdMap map[string]int64, messages core.JsonMessages) *list.List {
	ret := list.New()
	if len(messages) == 0 {
		return ret
	}
	// use channel to multithread parsing and convertion to sql
	values := make(chan string)
	defer close(values)
	for idx, _ := range messages {
		go func(idx int) {
			tick, exists := getTickFromMessage(symbolToIdMap, messages[idx])
			if exists {
				values <- tick.toSqlValues()
			} else {
				values <- ""
			}
		}(idx)
	}
	// Copy channel to
	chanCnt := 0
	for v := range values {
		if v != "" {
			ret.PushBack(v)
		}
		chanCnt += 1
		if chanCnt == len(messages) {
			// exit when all values have been read
			break
		}
	}
	return ret
}

func convertMessagesToSql(symbolToIdMap map[string]int64, messages core.JsonMessages) (string, int) {
	values := getValuesFromMessages(symbolToIdMap, messages)
	if values.Len() == 0 {
		return "", 0
	}
	var buffer bytes.Buffer
	buffer.WriteString(`
		INSERT DELAYED INTO xtb__ticks
		(
			timestamp    ,
			ticker_id    ,
			ask          ,
			ask_v        ,
			bid          ,
			bid_v        ,
			level        ,
			quote_id     
		 ) VALUES
		 `)
	for v := values.Front(); v != nil; {
		str, _ := v.Value.(string)
		buffer.WriteString(str)
		v = v.Next()
		if v != nil {
			buffer.WriteString(",")
		}
	}
	buffer.WriteString(";")
	return buffer.String(), values.Len()
}
