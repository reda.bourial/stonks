FROM golang:bullseye

WORKDIR /usr/src/app
ARG SSH_PRIVATE_KEY
RUN mkdir -p /root/.ssh

# Setup ssh key for importing private libraries 
RUN echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa
RUN chmod 700 -R /root/.ssh
RUN mkdir -p /etc/ssh/
RUN echo "    IdentityFile /root/.ssh/id_rsa" >> /etc/ssh/ssh_config
RUN ssh-keyscan -t rsa git.artisandunet.com >> /root/.ssh/known_hosts
RUN ssh-keyscan -t rsa gitlab.com >> /root/.ssh/known_hosts
RUN ssh-keyscan -t rsa github.com >> /root/.ssh/known_hosts
COPY . /usr/src/app/
RUN go install 
