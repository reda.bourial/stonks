package xtb

import (
	"fmt"

	"github.com/tidwall/gjson"
	"gitlab.com/reda.bourial/stonks/core"
)

type Broker struct {
	accounts           map[uint64]core.Account
	defaultRealAccount core.Account
	defaultDemoAccount core.Account
}

func (b *Broker) Name() string {
	return "xtb"
}

func (b *Broker) GetAccounts() map[uint64]core.Account {
	return b.accounts
}

func (b *Broker) GetDefaultAccount() core.Account {
	return b.defaultRealAccount
}

func (b *Broker) GetDefaultDemo() core.Account {
	return b.defaultDemoAccount
}

func getAccounts(b *Broker, isDemo bool, params []gjson.Result) (accounts []core.Account, defaultAccount core.Account, err error) {
	accounts = []core.Account{}
	for _, account := range params {
		instance, isDefault, err := AccountFactory(account, false)
		if err != nil {
			return nil, nil, err
		}
		if isDefault {
			if defaultAccount == nil {
				defaultAccount = instance
			} else {
				err = fmt.Errorf(
					"multiple default real accounts %s %s",
					defaultAccount.Name(), instance.Name(),
				)
				return nil, nil, err
			}
		}
		accounts = append(accounts, instance)
	}
	return accounts, defaultAccount, nil
}

func BrokerFactory(config gjson.Result) (core.Broker, error) {
	realAccountsParams := config.Get("accounts").Get("real").Array()
	demoAccountsParams := config.Get("accounts").Get("demo").Array()
	broker := &Broker{
		accounts: map[uint64]core.Account{},
	}

	accounts, defaultAccount, err := getAccounts(broker, false, realAccountsParams)
	if err != nil {
		return nil, err
	}
	broker.defaultRealAccount = defaultAccount
	for _, a := range accounts {
		broker.accounts[a.Hash()] = a
	}
	accounts, defaultAccount, err = getAccounts(broker, true, demoAccountsParams)
	if err != nil {
		return nil, err
	}
	broker.defaultDemoAccount = defaultAccount
	for _, a := range accounts {
		broker.accounts[a.Hash()] = a
	}
	return broker, nil
}
