package xtb

import "gitlab.com/reda.bourial/stonks/core"

var migrations = core.Migrations{
	core.MakeMigration([]string{
		`
		create table xtb__tickers__migrations
		(
			id          int auto_increment PRIMARY KEY ,
			applied_on  TIMESTAMP not null DEFAULT CURRENT_TIMESTAMP,
			` + "`before`" + `      TEXT,
			` + "`after`" + `       TEXT,
			CHECK ( JSON_VALID(` + "`before`" + `)  AND JSON_VALID(` + "`after`" + `))
		);

		`,
		`ALTER table xtb__tickers__migrations engine=MYISAM;`,
	}),
	core.MakeMigration([]string{
		`
		create table xtb__tickers__data
		(
			id              int NOT NULL AUTO_INCREMENT,
			symbol          varchar(64),
			group_name      varchar(64),
			description     text,
			currency        varchar(16),
			currency_profit varchar(16),
			category_name   varchar(32),
			margin_mode     int,
			leverage        float,
			lot_min         float,
			tick_size       float,
			tick_value      float,
			swap_long       float,
			swap_short      float,
			long_only       bool,
			rawJson         text,
			marketHours     text,
			PRIMARY KEY (id)
		);
	`,
		`
		ALTER TABLE xtb__tickers__data 
		ADD CONSTRAINT XtbTickersDataUniqueSymbol UNIQUE(symbol);
	`,
	}),
	core.MakeMigration([]string{
		`
		create table xtb__ticks
		(
			id int auto_increment,
			created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
			ticker_id int not null,
			timestamp bigint not null,
			ask float not null,
			ask_v int not null,
			bid float not null,
			bid_v int not null,
			level smallint not null,
			quote_id smallint null,
			spread_raw float null,
			spread_table float null,
			PRIMARY KEY (id)
		);
	`,
		`ALTER TABLE xtb__ticks engine=MYISAM;`,
		`create unique index xtb__ticks_unique
			on xtb__ticks (timestamp,ticker_id,level,quote_id);`,
	}),
	core.MakeMigration([]string{
		`alter table xtb__ticks modify ask float null;`,
		`alter table xtb__ticks modify ask_v int null;`,
		`alter table xtb__ticks modify bid float null;`,
		`alter table xtb__ticks modify bid_v int null;`,
		`alter table xtb__ticks modify level smallint null;`,
		`alter table xtb__ticks modify quote_id smallint null;`,
		`alter table xtb__ticks modify spread_raw float null;`,
		`alter table xtb__ticks modify spread_table float null;`,
	}),
	core.MakeMigration([]string{
		`alter table xtb__ticks modify ask_v bigint null;`,
		`alter table xtb__ticks modify bid_v bigint null;`,
	}),
	core.MakeMigration([]string{
		`alter table xtb__ticks modify ticker_id mediumint unsigned not null;`,
		`alter table xtb__ticks modify level tinyint unsigned null;`,
		`alter table xtb__ticks modify quote_id tinyint unsigned null;`,
		`alter table xtb__ticks drop column spread_raw;`,
		`alter table xtb__ticks drop column spread_table;`,
	}),
}
