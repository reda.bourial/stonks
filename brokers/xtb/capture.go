package xtb

import (
	"database/sql"
	"time"

	"gitlab.com/reda.bourial/catch"
	"gitlab.com/reda.bourial/log4g"
	"gitlab.com/reda.bourial/stonks/core"
)

func listenToTickers(logger log4g.Logger, instruments []Instrument, account Account, db *sql.DB) {
	logger.Info("connected")
	symbolToIdMap := map[string]int64{}
	for idx, i := range instruments {
		symbolToIdMap[i.Symbol] = i.Id
		err := account.subscribeToTicker(i.GetTicker())
		logger.Trace("subscribing to", idx+1, "/", len(instruments), i.GetTicker())
		core.PanicIfError(err)
		time.Sleep(500 * time.Microsecond)
	}
	logger.Info("finished subscribing", account.IsAlive())
	core.LogEvent(db, core.Event{
		Plugin: "xtb",
		Code:   "CAPTURE_START",
	})
	for account.IsAlive() {
		messages, err := account.StreamConnection.GetJsonMessages()
		core.PanicIfError(err)
		messagesLen := len(messages)
		query, n := convertMessagesToSql(symbolToIdMap, messages)
		if n != 0 {
			core.LogEvent(db, core.Event{
				Plugin: "xtb",
				Code:   "CAPTURE_DATA",
				Data: []int{
					n,
				},
			})
			logger.Trace("inserting", messagesLen, "ticks")
			_, err := db.Exec(query)
			core.PanicIfError(err)
		} else {
			// Do not hog cpu time
			time.Sleep(200 * time.Microsecond)
		}
	}
	core.PanicIfError(account.Error())
}

func capture(d core.DiskCaptureContext, remainingArgs []string) {
	logger := log4g.NewConsoleLogger().PrependGoRoutines().PrependTime().PrependString(core.RunID())
	db, err := core.GetDatabase(&core.DatabaseOptions{
		Multistatement: true,
	})
	core.PanicIfError(err)
	instruments, err := getInstruments(db)
	core.PanicIfError(err)
	account := getDefaultAccount()
	fn := catch.SanitizeFunc(func() {
		listenToTickers(logger, instruments, *account, db)
	})
	core.LogEvent(db, core.Event{
		Plugin: "xtb",
		Code:   "CAPTURE_INIT",
		Data: map[string]interface{}{
			"instruments": instruments,
		},
	})
	for {
		_, err := fn()
		for err != nil {
			core.LogEvent(db, core.Event{
				Plugin: "xtb",
				Code:   "CAPTURE_ERROR",
				Data: map[string]interface{}{
					"error": err,
				},
			})
			logger.Error(err)
			err = account.Reconnect()
		}
	}
}
