package core

import (
	"container/list"
	"encoding/json"
	"sync"

	"github.com/gorilla/websocket"
	"github.com/tidwall/gjson"
	"gitlab.com/reda.bourial/catch"
)

func Dial() {

}

type Message struct {
	MessageType int
	Data        []byte
}

func (m Message) ToJson() gjson.Result {
	return gjson.Parse(string(m.Data))
}

type Messages []Message
type JsonMessages []gjson.Result

type WebsocketConnection struct {
	Conn       *websocket.Conn
	Url        string
	ReadErr    error
	WriteErr   error
	WriteMutex sync.Mutex
	onMessage  func(Message, error)
}

func (con *WebsocketConnection) Error() error {
	if err := con.ReadErr; err != nil {
		return err
	}
	return con.WriteErr
}

func (con *WebsocketConnection) Write(obj interface{}) error {
	data, err := json.Marshal(obj)
	if err != nil {
		return err
	}
	con.WriteMutex.Lock()
	defer con.WriteMutex.Unlock()
	err = con.Conn.WriteMessage(1, data)
	if err != nil {
		con.WriteErr = err
		return err
	}
	return nil
}

func (con *WebsocketConnection) listen() {
	for con.Error() == nil {
		tipe, message, err := con.Conn.ReadMessage()
		if err != nil {
			con.ReadErr = err
			return
		} else {
			con.onMessage(Message{tipe, message}, err)
		}
	}
}
func (con *WebsocketConnection) Reconnect() error {
	con.WriteMutex.Lock()
	defer con.WriteMutex.Unlock()
	closeFunc := catch.SanitizeFunc(con.Conn.Close)
	defer closeFunc()
	c, resp, err := websocket.DefaultDialer.Dial(con.Url, nil)
	if err != nil {
		return err
	}
	closeResp := catch.SanitizeFunc(resp.Body.Close)
	defer closeResp()
	con.Conn = c
	con.WriteErr = nil
	con.ReadErr = nil
	go con.listen()
	return nil
}

func GetConnection(url string, onMessage func(Message, error)) (*WebsocketConnection, error) {
	c, resp, err := websocket.DefaultDialer.Dial(url, nil)
	closeResp := catch.SanitizeFunc(resp.Body.Close)
	defer closeResp()

	if err != nil {
		panic(err)
	}

	connection := &WebsocketConnection{
		Conn:      c,
		Url:       url,
		onMessage: onMessage,
	}
	go connection.listen()
	return connection, nil
}

func GetStreamedWebsocket(url string) (*WebsocketConnection, chan Message, error) {
	ret := make(chan Message)
	onMessage := func(m Message, err error) {
		if err == nil {
			ret <- m
		}
	}
	connection, err := GetConnection(url, onMessage)
	return connection, ret, err
}

func convertListToMessages(l *list.List) Messages {
	if l.Len() == 0 {
		return Messages{}
	}
	messages := make(Messages, l.Len())
	idx := 0
	for m := l.Front(); m != nil; m = m.Next() {
		message, _ := m.Value.(Message)
		messages[idx] = message
		idx += 1
	}
	return messages
}

type BufferedConnection struct {
	*WebsocketConnection
	messagesLock *sync.Mutex
	messages     *list.List
}

func (ws *BufferedConnection) popMessages() *list.List {
	// do not reallocate or lock
	// if no message is found
	if ws.messages.Len() == 0 {
		return list.New()
	}
	ws.messagesLock.Lock()
	defer ws.messagesLock.Unlock()
	messages := ws.messages
	ws.messages = list.New()
	return messages
}

func (ws *BufferedConnection) GetMessages() (Messages, error) {
	if err := ws.Error(); err != nil {
		return Messages{}, err
	}
	messages := ws.popMessages()
	return convertListToMessages(messages), nil
}

func (ws *BufferedConnection) GetJsonMessages() (JsonMessages, error) {
	messages, err := ws.GetMessages()
	if err != nil {
		return JsonMessages{}, err
	}
	jsonMessages := make(JsonMessages, len(messages))
	wg := sync.WaitGroup{}
	wg.Add(len(messages))
	for idx, _ := range messages {
		go func(idx int) {
			defer wg.Done()
			message := messages[idx].ToJson()
			jsonMessages[idx] = message
		}(idx)
	}
	wg.Wait()
	return jsonMessages, nil
}

func (ws *BufferedConnection) receiveMessage(m Message) {
	ws.messagesLock.Lock()
	defer ws.messagesLock.Unlock()

	ws.messages.PushBack(m)
}

func GetBufferedConnection(url string) (*BufferedConnection, error) {
	ws, messageChan, err := GetStreamedWebsocket(url)
	if err != nil {
		return nil, err
	}
	bws := &BufferedConnection{
		ws,
		&sync.Mutex{},
		list.New(),
	}
	go func() {
		for message := range messageChan {
			bws.receiveMessage(message)
		}
	}()
	return bws, nil
}
