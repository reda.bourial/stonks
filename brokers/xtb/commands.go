package xtb

import (
	"fmt"

	"gitlab.com/reda.bourial/stonks/core"
	"gitlab.com/reda.bourial/yagclif/v2"
)

var commands = yagclif.Routes{
	{
		Name:        "version",
		Description: "print out version",
		Callback: func(remainingArgs []string) {
			if len(remainingArgs) > 0 {
				panic(fmt.Sprint("unknown arguments", remainingArgs))
			}
			fmt.Println("xtb version", "2.5.0")
		},
	},
	{
		Name:        "printaccounts",
		Description: "prints the accounts hashes",
		Callback: func(remainingArgs []string) {
			broker, _ := core.GetBroker("xtb")
			for _, account := range broker.GetAccounts() {
				fmt.Println(account.IsDemo(), "->", account.Name(), "->", account.Hash())
			}
		},
	},
	{
		Name:        "synctickers",
		Description: "syncs the accounts with the database",
		Callback:    syncTickers,
	},
	{
		Name:        "capture",
		Description: "Listens to markets and saves ticks to database",
		Callback:    capture,
	},
}
