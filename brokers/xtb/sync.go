package xtb

import (
	"gitlab.com/reda.bourial/log4g"
	"gitlab.com/reda.bourial/stonks/core"
)

func getDefaultAccount() *Account {
	broker, _ := core.GetBroker("xtb")
	defaultAccount := broker.GetDefaultAccount()
	connection, ok := defaultAccount.(*Account)
	if !ok {
		panic("cannot convert")
	}
	return connection
}

func syncTickers(remainingArgs []string) {
	account := getDefaultAccount()
	db, err := core.GetDatabase(nil)
	core.PanicIfError(err)
	instruments := account.getAllSymbols().Get("returnData").Array()
	nbInstruments := len(instruments)
	logger := log4g.NewConsoleLogger().PrependTime()
	// TODO pimp to show changes
	for idx, value := range instruments {
		logger.Info(idx+1, "/", nbInstruments, "done")
		_, err := syncInstrument(value, db)
		if err != nil {
			panic(err)
		}
	}
}
