package core

import (
	"database/sql"
	"encoding/json"
	"fmt"

	"github.com/google/uuid"
)

var run_id = fmt.Sprint(uuid.New())

type Event struct {
	Plugin string
	Code   string
	Data   interface{}
}

func (e Event) DataAsString() (string, error) {
	if e.Data == nil {
		return "{}", nil
	} else {
		bData, err := json.Marshal(e.Data)
		return string(bData), err
	}
}

func LogEvent(db *sql.DB, e Event) error {
	data, err := e.DataAsString()
	if err != nil {
		return nil
	}
	_, err = db.Exec(`
	INSERT DELAYED INTO
    core__logs (run_id, plugin, code, data)
    VALUES (
            ?,
            ?,
            ?,
            ?
    );
	`, RunID(), e.Plugin, e.Code, data)
	return err
}

func RunID() string {
	return run_id
}
