package xtb

// Request is the container for remote commands
type Request struct {
	Command   string      `json:"command"`
	Arguments interface{} `json:"arguments"`
}

// LoginArgs represent login credentials
type LoginArgs struct {
	UserId   string `json:"userId"`
	Password string `json:"password"`
	AppId    string `json:"appId"`
	AppName  string `json:"appName"`
}
