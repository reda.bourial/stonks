package core

import "time"

type WindowWrapper struct {
	Duration       time.Duration
	EndPtr         uint64
	StartPtr       uint64
	LastKnownEvent uint64
}

type Indicator interface {
	Duration() time.Duration
	Values() []interface{}
	Increment(newTicks []Tick, obsoleteTicks []Tick)
}
